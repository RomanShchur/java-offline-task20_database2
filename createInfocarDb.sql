-- MySQL Workbench Synchronization
-- Generated: 2019-10-17 21:59
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: RamoZ

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

ALTER SCHEMA `infocar`  DEFAULT CHARACTER SET utf8  DEFAULT COLLATE utf8_general_ci ;

CREATE TABLE IF NOT EXISTS `infocar`.`user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `email` VARCHAR(25) NULL DEFAULT NULL,
  `password` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `infocar`.`manufacturer` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `brand` VARCHAR(45) NOT NULL,
  `country` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `infocar`.`car` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `model` VARCHAR(45) NOT NULL,
  `used_car_id` INT(11) NOT NULL,
  `proudction_year` YEAR NOT NULL,
  `length` INT(11) NOT NULL,
  `width` INT(11) NOT NULL,
  `height` INT(11) NOT NULL,
  `seats` INT(11) NOT NULL,
  `doors` INT(11) NOT NULL,
  `weight` INT(11) NOT NULL,
  `full_weight` INT(11) NOT NULL,
  `manufacturer_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `manufacturer_id`),
  INDEX `fk_car_manufacturer1_idx` (`manufacturer_id` ASC) VISIBLE,
  CONSTRAINT `fk_car_manufacturer1`
    FOREIGN KEY (`manufacturer_id`)
    REFERENCES `infocar`.`manufacturer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `infocar`.`engine` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `fuel` VARCHAR(45) NOT NULL,
  `volume` FLOAT(11) NOT NULL,
  `cylinders` INT(11) NOT NULL,
  `valves` INT(11) NOT NULL,
  `power` INT(11) NOT NULL,
  `maximum_speed` INT(11) NOT NULL,
  `fuel_consumption` FLOAT(11) NOT NULL,
  `car_id` INT(11) NOT NULL,
  `car_used_car_id` INT(11) NOT NULL,
  `car_car_stats_id` INT(11) NOT NULL,
  `car_manufacturer_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `car_id`, `car_used_car_id`, `car_car_stats_id`, `car_manufacturer_id`),
  INDEX `fk_engine_car1_idx` (`car_id` ASC, `car_used_car_id` ASC, `car_car_stats_id` ASC, `car_manufacturer_id` ASC) VISIBLE,
  CONSTRAINT `fk_engine_car1`
    FOREIGN KEY (`car_id` , `car_used_car_id` , `car_manufacturer_id`)
    REFERENCES `infocar`.`car` (`id` , `used_car_id` , `manufacturer_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `infocar`.`gearbox` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `gearbox_type` VARCHAR(45) NOT NULL,
  `is_mechanical` TINYINT(4) NOT NULL,
  `gear_number` INT(11) NOT NULL,
  `wheel_drive` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `infocar`.`used_car` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `celler_phone` VARCHAR(45) NOT NULL,
  `price` INT(11) NOT NULL,
  `user_id` INT(11) NOT NULL,
  `car_id` INT(11) NOT NULL,
  `car_used_car_id` INT(11) NOT NULL,
  `car_car_stats_id` INT(11) NOT NULL,
  `city` VARCHAR(45) NOT NULL,
  `mileage` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_used_car_user_idx` (`user_id` ASC) VISIBLE,
  INDEX `fk_used_car_car1_idx` (`car_id` ASC, `car_used_car_id` ASC, `car_car_stats_id` ASC) VISIBLE,
  CONSTRAINT `fk_used_car_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `infocar`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_used_car_car1`
    FOREIGN KEY (`car_id` , `car_used_car_id`)
    REFERENCES `infocar`.`car` (`id` , `used_car_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `infocar`.`destributor` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL,
  `address` VARCHAR(45) NOT NULL,
  `phone` VARCHAR(45) NOT NULL,
  `city` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `infocar`.`car_has_destributor` (
  `price` INT(11) NOT NULL,
  `car_id` INT(11) NOT NULL,
  `car_used_car_id` INT(11) NOT NULL,
  `car_car_stats_id` INT(11) NOT NULL,
  `destributor_id` INT(11) NOT NULL,
  INDEX `fk_car_has_destributor_car1_idx` (`car_id` ASC, `car_used_car_id` ASC, `car_car_stats_id` ASC) VISIBLE,
  PRIMARY KEY (`destributor_id`),
  CONSTRAINT `fk_car_has_destributor_car1`
    FOREIGN KEY (`car_id` , `car_used_car_id`)
    REFERENCES `infocar`.`car` (`id` , `used_car_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_car_has_destributor_destributor1`
    FOREIGN KEY (`destributor_id`)
    REFERENCES `infocar`.`destributor` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `infocar`.`engine_has_gearbox` (
  `engine_id` INT(11) NOT NULL,
  `gearbox_id` INT(11) NOT NULL,
  PRIMARY KEY (`engine_id`, `gearbox_id`),
  INDEX `fk_engine_has_gearbox_gearbox1_idx` (`gearbox_id` ASC) VISIBLE,
  INDEX `fk_engine_has_gearbox_engine1_idx` (`engine_id` ASC) VISIBLE,
  CONSTRAINT `fk_engine_has_gearbox_engine1`
    FOREIGN KEY (`engine_id`)
    REFERENCES `infocar`.`engine` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_engine_has_gearbox_gearbox1`
    FOREIGN KEY (`gearbox_id`)
    REFERENCES `infocar`.`gearbox` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

INSERT INTO `mydb`.`user` (`username`, `password`) VALUES ('user01', '1231');
INSERT INTO `mydb`.`user` (`username`, `password`) VALUES ('user02', '1111');
INSERT INTO `mydb`.`user` (`username`, `email`, `password`) VALUES ('newuser', 'mail@mail.mail', '1122');

INSERT INTO `mydb`.`manufacturer` (`brand`, `country`) VALUES ('BMW', 'Germany');
INSERT INTO `mydb`.`manufacturer` (`brand`, `country`) VALUES ('Toyota', 'Japan');
INSERT INTO `mydb`.`manufacturer` (`brand`, `country`) VALUES ('Ford', 'USA');
INSERT INTO `mydb`.`manufacturer` (`brand`, `country`) VALUES ('ZAZ', 'Ukraine');

INSERT INTO `mydb`.`car` (`model`, `used_car_id`, `production_year`, `length`, `width`, `height`, `seats`, `doors`, `weight`, `full_weight`, `manufacturer_id`) VALUES ('M3', '1','1999','4200','2100','1500','2','3','1500','2500','1',);
INSERT INTO `mydb`.`car` (`model`, `used_car_id`, `proudction_year`, `length`, `width`, `height`, `seats`, `doors`, `weight`, `full_weight`, `manufacturer_id`) VALUES ('Corolla', '0', 2004, '3900', '2000', '1400', '4', '5', '1300', '2600', '2');
INSERT INTO `mydb`.`car` (`model`, `used_car_id`, `proudction_year`, `length`, `width`, `height`, `seats`, `doors`, `weight`, `full_weight`, `manufacturer_id`) VALUES ('Mustang', '0', 1967, '5200', '2400', '1600', '2', '3', '2000', '2900', '3');
INSERT INTO `mydb`.`car` (`model`, `used_car_id`, `proudction_year`, `length`, `width`, `height`, `seats`, `doors`, `weight`, `full_weight`, `manufacturer_id`) VALUES ('Zapor', '0', 1982, '2400', '1000', '1200', '4', '3', '900', '1500', '4');

INSERT INTO `mydb`.`destributor` (`title`, `address`, `phone`, `city`) VALUES ('ford official', '0.000', '+1234567', 'Kiyiv');
INSERT INTO `mydb`.`destributor` (`title`, `address`, `phone`, `city`) VALUES ('Toyota official', '0.000', '+11122', 'Lviv');
INSERT INTO `mydb`.`destributor` (`title`, `address`, `phone`, `city`) VALUES ('BMW official', '0.000', '+354123', 'Lviv');
INSERT INTO `mydb`.`destributor` (`title`, `address`, `phone`, `city`) VALUES ('Zaz', '0.000', '+000000', 'Zapor');

INSERT INTO `mydb`.`engine` (`fuel`, `volume`, `cylinders`, `valves`, `power`, `maximum_speed`, `fuel_consumption`, `car_car_stats_id`, `car_id`, `car_manufacturer_id`) VALUES ('disel', '1.4', '6', '12', '200', '230', '1.5', '0', '2', '2');
INSERT INTO `mydb`.`engine` (`fuel`, `volume`, `cylinders`, `valves`, `power`, `maximum_speed`, `fuel_consumption`, `car_car_stats_id`, `car_id`, `car_manufacturer_id`) VALUES ('gas', '2.5', '6', '12', '240', '300', '3.0', '0', '1', '1');
INSERT INTO `mydb`.`engine` (`fuel`, `volume`, `cylinders`, `valves`, `power`, `maximum_speed`, `fuel_consumption`, `car_car_stats_id`, `car_id`, `car_manufacturer_id`) VALUES ('gas', '2.5', '8, '16', '320', '320', '3.8', '0', '1', '1');
INSERT INTO `mydb`.`engine` (`fuel`, `volume`, `cylinders`, `valves`, `power`, `maximum_speed`, `fuel_consumption`, `car_car_stats_id`, `car_id`, `car_manufacturer_id`) VALUES ('wood', '0.2', '1', '2', '20', '0', '0.0', '0', '4', '4');

INSERT INTO `mydb`.`gearbox` (`gearbox_type`, `is_mechanical`, `gear_number`, `wheel_drive`) VALUES ('mechanical', '1', '5', 'back');
INSERT INTO `mydb`.`gearbox` (`gearbox_type`, `is_mechanical`, `gear_number`, `wheel_drive`) VALUES ('electrical', '0', '4', 'front');
INSERT INTO `mydb`.`gearbox` (`gearbox_type`, `is_mechanical`, `gear_number`, `wheel_drive`) VALUES ('mechanical', '1', '5', 'front');
INSERT INTO `mydb`.`gearbox` (`gearbox_type`, `is_mechanical`, `gear_number`, `wheel_drive`) VALUES ('mechanical', '1', '4', 'back');
